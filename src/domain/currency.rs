#[derive(Eq, PartialEq, Debug, Copy, Clone)]
pub enum CurrencyCode {
    EUR,
    USD,
}