use std::cmp::Ordering;
use std::ops::{Add, Sub};
use crate::domain::currency::CurrencyCode;
use crate::domain::money::Error::{DifferingCurrencies, NegativeResult};

#[derive(PartialEq, Debug)]
pub struct Money {
    amount: u32,
    currency: CurrencyCode,
}

impl Money {
    fn new(amount: u32, currency: CurrencyCode) -> Money {
        Money { amount, currency }
    }
}

impl PartialOrd for Money {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        if self.currency == other.currency {
            Some(self.amount.cmp(&other.amount))
        } else {
            None
        }
    }
}

#[derive(PartialEq, Debug)]
pub enum Error {
    DifferingCurrencies,
    NegativeResult,
}

impl Add for Money {
    type Output = Result<Money, Error>;

    fn add(self, rhs: Self) -> Self::Output {
        require_same_currency(&self, &rhs)?;
        Ok(Money::new(self.amount + rhs.amount, self.currency))
    }
}

impl Sub for Money {
    type Output = Result<Money, Error>;

    fn sub(self, rhs: Self) -> Self::Output {
        require_same_currency(&self, &rhs)?;
        if self.amount > rhs.amount {
            Ok(Money::new(self.amount - rhs.amount, self.currency))
        } else {
            Err(NegativeResult)
        }
    }
}

fn require_same_currency(one: &Money, other: &Money) -> Result<(), Error> {
    if one.currency == other.currency {
        Ok(())
    } else {
        Err(DifferingCurrencies)
    }
}

#[cfg(test)]
pub(crate) mod test {
    use super::*;
    use fluent_asserter::prelude::*;

    mod eq {
        use super::*;

        #[test]
        fn should_equal_money_with_same_amount_and_currency() {
            assert_that!(euros(300)).is_equal_to(euros(300))
        }

        #[test]
        fn should_not_equal_money_with_different_amount() {
            assert_that!(euros(300)).is_not_equal_to(euros(100))
        }

        #[test]
        fn should_not_equal_money_with_different_currency() {
            assert_that!(euros(300)).is_not_equal_to(dollars(300))
        }
    }

    mod add {
        use super::*;

        #[test]
        fn should_add_money_of_same_currency() {
            assert_that!(euros(500) + euros(300)).is_equal_to(Ok(euros(800)))
        }

        #[test]
        fn should_not_add_money_with_different_currency() {
            assert_that!(euros(500) + dollars(300)).is_equal_to(Err(DifferingCurrencies))
        }
    }

    mod sub {
        use super::*;

        #[test]
        fn should_subtract_smaller_money_of_same_currency() {
            assert_that!(euros(500) - euros(150)).is_equal_to(Ok(euros(350)))
        }

        #[test]
        fn should_not_subtract_money_with_different_currency() {
            assert_that!(euros(500) - dollars(150)).is_equal_to(Err(DifferingCurrencies))
        }

        #[test]
        fn should_not_subtract_money_with_bigger_amount() {
            assert_that!(euros(100) - euros(300)).is_equal_to(Err(NegativeResult))
        }
    }

    mod partial_cmp {
        use super::*;

        #[test]
        fn should_compare_by_amount() {
            assert_that!(euros(100) < euros(101)).is_true();
            assert_that!(euros(100) < euros(100)).is_false();
        }

        #[test]
        fn should_not_compare_with_different_currencies() {
            assert_that!(euros(100) < dollars(101)).is_false();
        }
    }

    pub(crate) fn euros(amount: u32) -> Money {
        Money::new(amount, CurrencyCode::EUR)
    }

    pub(crate) fn dollars(amount: u32) -> Money {
        Money::new(amount, CurrencyCode::USD)
    }
}